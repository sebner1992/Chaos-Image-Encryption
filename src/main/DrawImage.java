package main;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public class DrawImage extends JPanel {
	BufferedImage image;

	public DrawImage(BufferedImage image) {
		this.image = image;
	}

	public void paintComponent(Graphics g) {
		g.drawImage(image, 0, 0, null, this);
	}

	public void changeImage(BufferedImage image) {
		this.image = image;
		this.repaint();
	}
}
