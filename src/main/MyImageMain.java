package main;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class MyImageMain {
	private static ImageHandler imageHandler = new ImageHandler();
	private static boolean keyChecker = false;
	private static String dialogMessage;
	private static String title;
	
	/**
	 * Configurations
	 */
	private static String MAP = "BAKER";
	private static int waitBetweenSteps = 2000;
	private static int waitBetweenLoops = 5000;

	public static void main(String[] args) throws IOException {

		//reading image, getting max sidelength
		BufferedImage image = readImage();
		BufferedImage newImage;
		int size = Math.max(image.getHeight(), image.getWidth());
		
		int[] key = null;

		// Change size to have a certain number of divisors
		while (numberOfDivisors(size) < 10)
			size++;

		dialogMessage = "Enter Key for size: " + size + "\n" + "(example for size 600: 100 200 300)\n"
				+ "Usable numbers: ";
		dialogMessage = addDivisors(dialogMessage, size);

		//if image width != height, resize it to max size
		if (image.getWidth() != image.getHeight())
			newImage = imageHandler.adjustImageSize(image, size, size);
		else
			newImage = image;

		//Generated the image frame
		DrawImage drawImage = new DrawImage(image);
		JFrame frame = new JFrame();
		frame.setContentPane(drawImage);
		frame.setSize(newImage.getWidth(), newImage.getHeight());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);

		//Baker or Cat map
		if(MAP.equals("BAKER"))
		{
			//Select a key until its OK
			while (!keyChecker) {
				String tempKey = JOptionPane.showInputDialog(frame, dialogMessage);
				key = checkKey(tempKey, size);
			}
			//set the permutation
			imageHandler.setBakerMapPermutation(key, newImage.getWidth(), newImage.getHeight());
			title = "Bakers map";
		}
		else
		{
			//set the permutation
			imageHandler.setCatMapPermutation(1, 1, newImage.getWidth(), newImage.getHeight());
			title = "Cat map";
		}
		
		/**
		 * map encryption
		 */
		frame.setTitle(title);
		
		sleep(500);

		int i = 0;

		for (; i < 10; i++) {
			frame.setTitle(title + " encryption, iteration count: " + (i + 1));
			//updating the image after encryption
			drawImage.changeImage(encrypt(newImage));
			sleep(waitBetweenSteps);
		}

		sleep(waitBetweenLoops);

		for (; i < 20; i++) {
			frame.setTitle(title + " decryption, iteration count: " + (i + 1));
			drawImage.changeImage(decrypt(newImage));
			sleep(waitBetweenSteps);
		}

		sleep(waitBetweenLoops);

		for (; i < 30; i++) {
			frame.setTitle(title + " encryption, iteration count: " + (i + 1));
			drawImage.changeImage(decrypt(newImage));
			sleep(waitBetweenSteps);
		}

		sleep(waitBetweenLoops);

		for (; i < 40; i++) {
			frame.setTitle(title + " revers decryption, iteration count: " + i);
			drawImage.changeImage(encrypt(newImage));

			sleep(waitBetweenSteps);
		}
	}

	private static BufferedImage readImage() {
		try {
			return imageHandler.readImage();
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}

	private static BufferedImage encrypt(BufferedImage image) {
		try {
			return imageHandler.encrypt(image);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static BufferedImage decrypt(BufferedImage image) {
		try {
			return imageHandler.decrypt(image);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}

	private static void sleep(int milliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(milliseconds);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private static int[] checkKey(String key, int size) {
		int sum = 0;
		String[] strKey = key.split(",");
		int[] results = new int[strKey.length];

		for (int i = 1; i < strKey.length; i++) {
			results[i] = Integer.parseInt(strKey[i]);
			if (size % results[i] == 0) {
				sum += Integer.parseInt(strKey[i]);
			} else {
				return null;
			}
		}
		if (sum == size) {
			keyChecker = true;
			return results;
		}
		return null;

	}

	private static String addDivisors(String s, int number) {
		for (int i = 1; i <= number / 2; i++) {
			if (number % i == 0) {
				s = s + i + " ";
			}
		}
		return s;
	}

	private static int numberOfDivisors(int number) {
		int divisors = 0;

		for (int i = 1; i <= number / 2; i++) {
			if (number % i == 0) {
				divisors++;
			}
		}
		return divisors;
	}
}
