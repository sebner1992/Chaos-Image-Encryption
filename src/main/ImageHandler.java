package main;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class ImageHandler {
	
	//global variables for encryption/decryption
	int[] copy;
	int[] encryption_permutation;
	int[] decryption_permutation;

	/**
	 * Read Image
	 * 
	 * @throws IOException
	 */
	public BufferedImage readImage() throws IOException {
		return ImageIO.read(this.getClass().getResource("/images/img.jpg"));
	}

	/**
	 * Write Image
	 * 
	 * @throws IOException
	 */
	public void writeImage(BufferedImage image, String fileName) throws IOException {
		File f = new File("Source Folder\\images\\output\\" + fileName + ".jpg");
		ImageIO.write(image, "jpg", f);
	}

	/**
	 * Compares two images
	 *
	 * @param image first image
	 * @param image2 second image
	 * @return true if they match
	 */
	public boolean compareImages(BufferedImage image1, BufferedImage image2) {
		if (image1.getHeight() != image2.getHeight() || image1.getWidth() != image2.getWidth()) {
			System.out.println("Wrong dimension!");
			return false;
		}
		for (int x = 0; x < image1.getWidth(); x++) {
			for (int y = 0; y < image1.getHeight(); y++) {
				if (image1.getRGB(x, y) != image2.getRGB(x, y)) {
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * Encrypts a given image using the set encrypt permutation
	 * @param image
	 * @return the encrypted image
	 * @throws IOException
	 */
	public BufferedImage encrypt(BufferedImage image) throws IOException {
		
		setCopy(image);
		
		for(int x = 0; x < image.getHeight(); x++)
			for(int y = 0; y < image.getWidth(); y++)
				image.setRGB(x, y, copy[encryption_permutation[y + x * image.getHeight()]]);
	
		return image;
	}

	public BufferedImage decrypt(BufferedImage image) throws IOException {
		
		setCopy(image);
		
		for(int x = 0; x < image.getWidth(); x++)
			for(int y = 0; y < image.getWidth(); y++)
				image.setRGB(x, y, copy[decryption_permutation[y + x * image.getHeight()]]);
	
		return image;
	}

	/**
	 * Creates a copy of a given image
	 *
	 * @param image to make a copy of
	 */
	private void setCopy(BufferedImage image) {
		copy = new int[image.getWidth() * image.getHeight()];
		for (int x = 0; x < image.getHeight(); x++)
			for (int y = 0; y < image.getWidth(); y++)
				copy[y + x * image.getHeight()] = image.getRGB(x, y);

	}
	
	/**
	 * Set the encryption/decryption permutation in the 1-dimensional arrays
	 * encryption_permutation and decryption_permutation to a baker map
	 * permutation
	 *
	 * @param k_i key that defines the permutation
	 * @param width image width
	 * @param height image height
	 */
	public void setBakerMapPermutation(int[] k_i, int width, int height) {

		encryption_permutation = new int[width * height];
		decryption_permutation = new int[width * height];

		int new_x, new_y;

		int[] n = new int[k_i.length];
		for (int i = 1; i < n.length; i++)
			n[i] = n[i - 1] + k_i[i];
		int N = n[k_i.length - 1];
		int q_i;

		for (int i = 0; i < k_i.length - 1; i++) {
			for (int x = 0; x < height; x++) {
				if (n[i] <= x && x < n[i + 1]) {
					for (int y = 0; y < width; y++) {
						q_i = N / k_i[i + 1];

						new_x = q_i * (x - n[i]) + y % q_i;
						new_y = (y - y % q_i) / q_i + n[i];

						encryption_permutation[y + x * height] = new_y + new_x * height;
						decryption_permutation[new_y + new_x * height] = y + x * height;

					}
				}
			}
		}
	}
	
	/**
	 * Set the encryption/decryption permutation in the 1-dimensional arrays
	 * encryption_permutation and decryption_permutation to a cat map
	 * permutation
	 *
	 * @param p first key parameter that defines the permutation
	 * @param q second key parameter that defines the permutation
	 * @param width image width
	 * @param height image height
	 */
	public void setCatMapPermutation(int p, int q, int width, int height) throws IOException {

		encryption_permutation = new int[width * height];
		decryption_permutation = new int[width * height];
		
		int new_x, new_y;

		for (int x = 0; x < width; x++) {
			for (int y = 0; y < width; y++) {
				new_x = (x + y * p) % width;
				new_y = (q * x + (1 + p * q) * y) % width;

				encryption_permutation[y + x * height] = new_y + new_x * height;
				decryption_permutation[new_y + new_x * height] = y + x * height;
			}
		}
	}
	
	public BufferedImage adjustImageSize(BufferedImage image, int width, int height){
		BufferedImage newImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		
		for(int y = 0; y < image.getHeight(); y++)
			for(int x = 0; x < image.getWidth(); x++)
				newImage.setRGB(x, y, image.getRGB(x, y));
		
		return newImage;
	}
}
